const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/ 

// 1. Get all items that are available 

const availableItems = items.filter((item)=>{
    return item.available === true;
})
console.log(availableItems);


//    2. Get all items containing only Vitamin C.

const onlyVitaminC = items.filter((item) =>{
    return item.contains ==='Vitamin C';
})
console.log(onlyVitaminC);


//3. Get all items containing Vitamin A.

const VitaminA = items.filter((item) =>{
    let regex = /Vitamin A/i;
    return item.contains.match(regex);
})
console.log(VitaminA);


/* 4. Group items based on the Vitamins that they contain in the following format:
{
    "Vitamin C": ["Orange", "Mango"],
    "Vitamin K": ["Mango"],
}
and so on for all items and all Vitamins. */

const vitaminList = items.reduce((vitaminAccumulator, item) =>{
    let vitamins= item.contains;
    const vitaminArray = vitamins.split(', ');
   //console.log(vitamin +"  array is "+ vitaminArray);

    vitaminArray.map((tempvitamin) =>{
        if(tempvitamin in vitaminAccumulator){
           vitaminAccumulator[tempvitamin].push(item.name);
        }
        else{
            vitaminAccumulator[tempvitamin] = [item.name];
        }
     })
    return vitaminAccumulator;
},{})
console.log(vitaminList);

//    5. Sort items based on number of Vitamins they contain.

const sortedItems = items.sort((item1,item2)=>{
    let vitaminOfItem1 = item1.contains;
    let vitaminOfItem2 = item2.contains;
    
    const vitaminOfItem1Array = vitaminOfItem1.split(', ');
    const vitaminOfItem2Array = vitaminOfItem2.split(', ');

    return vitaminOfItem1Array.length - vitaminOfItem2Array.length;
})

console.log(sortedItems);